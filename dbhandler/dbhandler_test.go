package dbhandler

import (
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/remotejob/hugo_static/domains"
	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestGetSitePages(t *testing.T) {
	type args struct {
		session mgo.Session
		site    domains.Site
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	_site := domains.Site{"kvartira-tsentr.eu", "www", "variant0", "realestate", "ru_RU", ""}

	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0", args{*dbsession, _site}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			GetSitePages(tt.args.session, tt.args.site)
		})
	}
}
