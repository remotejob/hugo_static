package dbhandler

import (
	"log"

	"gitlab.com/remotejob/hugo_static/domains"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetSitePages(session mgo.Session, site domains.Site) []domains.Domain {

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("static_webs").C("sites")

	var results []domains.Domain

	query := []bson.M{bson.M{"$match": bson.M{"domain": site.Domain}}, bson.M{"$project": bson.M{"domain": 1, "hosts": bson.M{"$filter": bson.M{"input": "$hosts", "as": "host", "cond": bson.M{"$eq": []string{"$$host.host", site.Host}}}}}}}

	pipe := c.Pipe(query)

	err := pipe.All(&results)
	if err != nil {

		log.Fatal(err)
	}
	log.Println("len", len(results))
	// log.Println(results[0].Hosts)
	return results

}
