package createPath

import (
	"os"
	"path/filepath"
	"strings"
)

func Create(rootdir string, paths []string, extention string) string {

	filestr := rootdir
	for _, path := range paths {

		path0 := strings.Replace(strings.TrimSpace(path), "  ", " ", -1)
		path1 := strings.Replace(path0, " ", "-", -1)

		filestr = filestr + "/" + path1

	}

	source_dir := filepath.Dir(filestr)

	if _, err := os.Stat(source_dir); os.IsNotExist(err) {
		// path/to/whatever does not exis
		os.MkdirAll(source_dir, os.ModePerm)

	}

	return filestr + "." + extention
}
