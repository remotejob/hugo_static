package getSitesList

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"

	"gitlab.com/remotejob/hugo_static/domains"
)

func GetList(file string) []domains.Site {

	var sites []domains.Site

	fcsv, err := os.Open(file)

	if err != nil {

		panic(err.Error())
	} else {

		r := csv.NewReader(bufio.NewReader(fcsv))
		r.Comma = ','

		for {
			record, err := r.Read()

			if err == io.EOF {
				break
			}

			site := domains.Site{record[0], record[1], record[2], record[3], record[4], record[5], record[6]}

			sites = append(sites, site)

		}

	}
	return sites
}
