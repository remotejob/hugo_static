package main

import (
	"os"
	"time"

	"log"

	"gitlab.com/remotejob/hugo_static/createMdPage"
	"gitlab.com/remotejob/hugo_static/createPath"
	"gitlab.com/remotejob/hugo_static/dbhandler"
	"gitlab.com/remotejob/hugo_static/domains"
	"gitlab.com/remotejob/hugo_static/getOnlyPages"
	"gitlab.com/remotejob/hugo_static/getSitesList"
	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

var sites []domains.Site

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}

	sites = getSitesList.GetList("sites.csv")
}
func main() {

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	for _, site := range sites {

		log.Println(site)
		domains := dbhandler.GetSitePages(*dbsession, site)

		pages := getOnlyPages.GetAll(domains)

		// log.Println(pages)

		for _, page := range pages {

			filestr := createPath.Create(site.Rootdir, page.Paths, "md")
			log.Println(filestr)

			createMdPage.CreateMd(filestr, page)

		}

	}

}
