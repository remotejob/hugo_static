package createMdPage

import (
	"log"
	"os"
	"testing"
	"time"

	mgo "gopkg.in/mgo.v2"

	"gitlab.com/remotejob/hugo_static/createPath"
	"gitlab.com/remotejob/hugo_static/dbhandler"
	"gitlab.com/remotejob/hugo_static/domains"
	"gitlab.com/remotejob/hugo_static/getOnlyPages"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}

func TestCreateMd(t *testing.T) {
	type args struct {
		mdfile string
		page   domains.Page
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	_site := domains.Site{"kuplyu.net", "www", "variant0", "realestate", "ru_RU", ""}

	arg_domain := dbhandler.GetSitePages(*dbsession, _site)

	pages := getOnlyPages.GetAll(arg_domain)

	filestr := createPath.Create("/home/juno/gowork/src/gitlab.com/remotejob/hugo_static/content/post", pages[0].Paths, "md")

	log.Println(filestr)

	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.

		// {"test0", args{"/home/juno/gowork/src/gitlab.com/remotejob/hugo_static/content/post/test.md", pages[0]}},
		{"test0", args{filestr, pages[0]}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			CreateMd(tt.args.mdfile, tt.args.page)
		})
	}
}
