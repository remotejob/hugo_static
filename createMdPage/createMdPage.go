package createMdPage

import (
	"bufio"
	"bytes"
	"encoding/json"
	"log"
	"os"

	"strings"

	"gitlab.com/remotejob/hugo_static/domains"
)

func CreateMd(mdfile string, page domains.Page) {

	var frontMatter domains.HugoFrontMatter

	frontMatter.Title = strings.Title(page.Title)
	frontMatter.Description = page.Keywords[4]
	frontMatter.Date = page.Sitemapdate
	frontMatter.Tags = page.Tags

	for i := 5; i < 10; i++ {
		frontMatter.Categories = append(frontMatter.Categories, page.Keywords[i])

	}

	frontMatterJson, _ := json.Marshal(frontMatter)

	// log.Println(jsonPrettyPrint(string(frontMatterJson)))

	f, err := os.Create(mdfile)
	if err != nil {

		log.Panicln(err)
	}

	strToWrite := jsonPrettyPrint(string(frontMatterJson)) + " " + page.Contents

	w := bufio.NewWriter(f)
	_, err = w.WriteString(string(strToWrite))
	if err != nil {

		log.Panicln(err)
	}

	w.Flush()

}

func jsonPrettyPrint(in string) string {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", "\t")
	if err != nil {
		return in
	}
	return out.String()
}
