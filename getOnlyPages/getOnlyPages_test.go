package getOnlyPages

import (
	"log"
	"os"
	"testing"
	"time"

	mgo "gopkg.in/mgo.v2"

	"gitlab.com/remotejob/hugo_static/dbhandler"
	"gitlab.com/remotejob/hugo_static/domains"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}

func TestGetAll(t *testing.T) {
	type args struct {
		_domain []domains.Domain
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	_site := domains.Site{"kuplyu.net", "www", "variant0", "realestate", "ru_RU", ""}

	arg_domain := dbhandler.GetSitePages(*dbsession, _site)

	tests := []struct {
		name string
		args args
		// want []domains.Page
	}{
		// TODO: Add test cases.
		{"test0", args{arg_domain}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// if got := GetAll(tt.args._domain); !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("GetAll() = %v, want %v", got, tt.want)
			// }
			got := GetAll(tt.args._domain)

			for _, page := range got {

				log.Println(page)
			}

		})
	}
}
