package getOnlyPages

import "gitlab.com/remotejob/hugo_static/domains"
import "log"

func GetAll(_domain []domains.Domain) []domains.Page {

	var pages []domains.Page
	if len(_domain) == 1 {

		pages = _domain[0].Hosts[0].Themes[0].Locales[0].Pages

	} else {

		log.Panicln("must be 1")

	}

	return pages

}
